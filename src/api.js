import {
	//
	ref,
	inject,
	provide,
	computed,
	reactive,
	watchEffect,
} from 'vue'

import mainQuery from './mainQuery.gql'

/** @type {import('vue').InjectionKey<ReturnType<typeof getAPI>>} */
const API_SYMBOL = Symbol('api')

/** @type {import('vue').InjectionKey<{ id: string, path: string }>} */
const PROJECT_SYMBOL = Symbol('project')

/** @type {import('vue').InjectionKey<import('vue').Ref<string>>} */
const TRACE_SYMBOL = Symbol('trace')

const HOST_KEY = 'gitlab-host'
const PATH_KEY = 'gitlab-path'
const TOKEN_KEY = 'gitlab-token'

const ctype = 'content-type'
const appJSON = 'application/json'

const getAPI = () => {
	const host = localStorage.getItem(HOST_KEY)
	if (!host) throw new Error('host not set')
	const token = localStorage.getItem(TOKEN_KEY)
	if (!token) throw new Error('token not set')
	const authorization = `Bearer ${token}`

	/**
	 * @template {unknown} T
	 * @param {string} query
	 * @return {Promise<T>}
	 */
	const gql = async (query) => {
		const resp = await fetch(`${host}/api/graphql`, {
			method: 'POST',
			body: JSON.stringify({ query }),
			headers: {
				authorization,
				[ctype]: appJSON,
			},
		})
		const { data, error } = await resp.json()
		if (error) {
			throw new Error(JSON.stringify(error))
		}
		return data
	}

	/**
	 * @template {unknown} T
	 * @param {string} path
	 * @param {RequestInit} opts
	 * @return {Promise<T>}
	 */
	const api = async (path, opts) => {
		const resp = await fetch(`${host}/api/v4/${path}`, {
			...opts,
			headers: {
				authorization,
				...opts?.headers,
			},
		})
		switch (resp.headers.get(ctype)) {
			case appJSON:
				return await resp.json()
			case 'text/plain':
				return await resp.text()
			default:
				return resp
		}
	}

	return {
		gql,
		v4: api,
	}
}

export const createAPI = () => {
	const state = reactive({
		host: localStorage.getItem(HOST_KEY),
		path: localStorage.getItem(PATH_KEY),
		token: localStorage.getItem(TOKEN_KEY),
	})

	const api = reactive(
		/** @type {ReturnType<typeof getAPI>} */ ({
			v4: () => {},
			gql: () => {},
		})
	)

	const trace = ref('')
	const projectId = ref('')
	const mergeRequests = ref([{ id: '' }])
	const projectPath = computed(() => `${state.host}/${state.path}`)

	/** @param {string} token */
	const setToken = (token) => {
		localStorage.setItem(TOKEN_KEY, token)
		state.token = token
	}

	/** @param {string} url */
	const setProject = (url) => {
		const { origin: host, pathname } = new URL(url)
		const path = pathname.slice(1)
		localStorage.setItem(HOST_KEY, host)
		localStorage.setItem(PATH_KEY, path)
		Object.assign(state, { host, path })
	}

	watchEffect(async () => {
		if (!state.host || !state.token) return
		Object.assign(api, getAPI())
		const r = await api.gql(mainQuery.replace(/PROJECT_PATH/g, state.path))
		projectId.value = r?.project?.id?.split('/')?.at(-1)
		mergeRequests.value = r?.currentUser?.authoredMergeRequests?.nodes ?? []
	})

	provide(TRACE_SYMBOL, trace)
	provide(API_SYMBOL, api)
	provide(
		PROJECT_SYMBOL,
		reactive({
			id: computed(() => parseInt(projectId.value)),
			path: projectPath,
		})
	)

	return {
		trace,
		setToken,
		setProject,
		projectPath,
		mergeRequests: computed(() =>
			mergeRequests.value.map((m) => /** @type string */ (m.id))
		),
	}
}

export const useAPI = () => {
	const api = inject(API_SYMBOL)
	if (!api) throw new Error('no api')
	return api
}

export const useProject = () => {
	const project = inject(PROJECT_SYMBOL)
	if (!project) throw new Error('no project')
	return project
}

export const useTrace = () => {
	const trace = inject(TRACE_SYMBOL)
	if (!trace) throw new Error('no trace')
	return trace
}
