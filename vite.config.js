import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import uno from 'unocss/vite'

/**
 * @param {string} code
 * @param {string} id
 */
const gqlTransform = (code, id) => {
	if (id.endsWith('.gql')) {
		return {
			code: `export default \`${code.replace(/\s+/g, ' ')}\``,
		}
	}
}

// https://vitejs.dev/config/
export default defineConfig({
	base: '/mr-dashboard/',
	plugins: [
		uno(),
		vue(),
		{ name: 'graphql-import', transform: gqlTransform },
	],
})
