/**
 * @typedef {{ id: string, name: string, status: string, started: Date }} CiJob
 * @typedef {{ id: string, name: string, status?: string, jobs?: CiJob[] }} CiStage
 */

export const statusIcon = {
	manual: '▶️',
	failed: '❌',
	running: '🌀',
	success: '✅',
	skipped: '⏩',
	pending: '⏺',
	created: '⏺',
	canceled: '🚫',
}

/** @param {unknown} stage */
export const parseStage = (stage) => {
	const { status, jobs: { nodes } } = stage ?? {}
	const jobs = nodes.map((j) => ({
		id: parseInt(j.id.split('/').at(-1)),
		name: j.name,
		status: j.status.toLowerCase(),
		started: new Date(j.startedAt),
	}))
	return {
		/** @type string */
		status,
		/** @type CiJob[] */
		jobs,
	}
}

/** @param {unknown} mergeRequest */
export const parseMR = (mergeRequest) => {
	const { pipelines, ...rest } = mergeRequest ?? {}
	const pp = pipelines.nodes[0]
	const pipeline = pp.id.split('/').at(-1)
	const stages = pp.stages.nodes.map((s) => ({
		id: s.id,
		name: s.name,
		...parseStage(s),
	}))

	return {
		...rest,
		/** @type string */
		pipeline,
		/** @type CiStage[] */
		stages,
	}
}
